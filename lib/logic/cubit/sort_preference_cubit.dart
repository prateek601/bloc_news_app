import 'package:bloc/bloc.dart';

part 'sort_preference_state.dart';

class SortPreferenceCubit extends Cubit<SortPreferenceState> {
  SortPreferenceCubit() : super(SortPreferenceState(sortPreference: 'Newest'));

  void emitSortPreference(String _sortPreference) =>
      emit(SortPreferenceState(sortPreference: _sortPreference));
}
