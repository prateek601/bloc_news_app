part of 'fetch_data_cubit.dart';

@immutable
abstract class FetchDataState {}

class FetchDataLoading extends FetchDataState {}

class FetchDataSuccess extends FetchDataState {
  final List<Article> article;
  FetchDataSuccess({required this.article});
}

class FetchDataError extends FetchDataState {}
