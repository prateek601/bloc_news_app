import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:bloc_news_app/constants.dart';
import 'package:bloc_news_app/data_model/news.dart';
import 'package:bloc_news_app/logic/cubit/home_scroll_controller_cubit.dart';
import 'package:bloc_news_app/logic/cubit/location_cubit.dart';
import 'package:bloc_news_app/logic/cubit/news_source_cubit.dart';
import 'package:bloc_news_app/logic/cubit/sort_preference_cubit.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

part 'fetch_data_state.dart';

class FetchDataCubit extends Cubit<FetchDataState> {
  LocationCubit locationCubit;
  NewsSourceCubit newsSourceCubit;
  int totalResults = 0;
  int totalPages = 1;
  int pageSize = 8;
  int page = 1;
  String url = '';
  bool reachedMaxScrollExtent = false;
  List<Article> articles = [];
  HomeScrollControllerCubit homeScrollControllerCubit;
  SortPreferenceCubit sortPreferenceCubit;

  FetchDataCubit({
    required this.locationCubit,
    required this.newsSourceCubit,
    required this.homeScrollControllerCubit,
    required this.sortPreferenceCubit,
  }) : super(FetchDataLoading()) {
    String _pageSize = pageSize.toString();
    url = 'https://newsapi.org/v2/top-headlines?country=in&pageSize=$_pageSize';
    makeGetRequest(url + '&page=1');
  }

  void monitorScrollController() {
    page++;
    if (kDebugMode) {
      print(page);
    }
    if (page <= totalPages) {
      homeScrollControllerCubit.emitAtMaxScrollExtent();
      reachedMaxScrollExtent = true;
      String _page = page.toString();
      String completeUrl = url + '&page=$_page';
      makeGetRequest(completeUrl);
    }
  }

  void fetchData() {
    emitFetchDataLoading();
    String _pageSize = pageSize.toString();
    page = 1;
    String _page = page.toString();
    if (kDebugMode) {
      print(newsSourceCubit.state.newsSource);
      print(locationCubit.state.location);
    }
    if (newsSourceCubit.state.newsSource.isEmpty) {
      String countryCode = countryMap[locationCubit.state.location]!;
      url =
          'https://newsapi.org/v2/top-headlines?country=$countryCode&pageSize=$_pageSize';
      String completeUrl = url + '&page=$_page';
      makeGetRequest(completeUrl);
    } else {
      String sources = '';
      List newsSource = newsSourceCubit.state.newsSource;
      for (int i = 0; i < newsSource.length; i++) {
        String selectedSource = newsSourceMap[newsSource[i]]!;
        if (i == newsSource.length - 1) {
          sources = sources + selectedSource;
        } else {
          sources = sources + selectedSource + ',';
        }
      }
      String sortBy =
          sortPreferenceMap[sortPreferenceCubit.state.sortPreference]!;
      url =
          'https://newsapi.org/v2/everything?sources=$sources&sortBy=$sortBy&pageSize=$_pageSize';
      String completeUrl = url + '&page=$_page';
      makeGetRequest(completeUrl);
    }
  }

  final Map<String, String> _headers = {
    'X-Api-Key': 'f193b8f1536f4d4caa9f6f6a92d5d304'
  };

  Future<void> makeGetRequest(String url) async {
    if (kDebugMode) {
      print(url);
    }
    try {
      http.Response response =
          await http.get(Uri.parse(url), headers: _headers);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseBody = json.decode(response.body);
        News news = News.fromJson(responseBody);
        if (homeScrollControllerCubit.state is AtMaxScrollExtent) {
          articles.addAll(news.articles);
          reachedMaxScrollExtent = false;
          homeScrollControllerCubit.emitNotAtMaxScrollExtent();
        } else {
          articles.clear();
          articles.addAll(news.articles);
          totalResults = news.totalResults;
          totalPages = (totalResults / pageSize).ceil();
          if (kDebugMode) {
            print(totalResults);
            print(totalPages);
          }
        }
        emitFetchDataSuccess(articles);
      } else {
        if (kDebugMode) {
          print(response.statusCode);
        }
        showError();
      }
    } catch (e) {
      if (kDebugMode) {
        print(e);
      }
      showError();
    }
  }

  void showError() {
    if (homeScrollControllerCubit.state is AtMaxScrollExtent) {
      homeScrollControllerCubit.emitNotAtMaxScrollExtent();
    } else {
      emitFetchDataError();
    }
  }

  emitFetchDataSuccess(List<Article> _article) =>
      emit(FetchDataSuccess(article: _article));

  emitFetchDataError() => emit(FetchDataError());

  emitFetchDataLoading() => emit(FetchDataLoading());
}
