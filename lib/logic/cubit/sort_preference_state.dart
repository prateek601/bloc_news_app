part of 'sort_preference_cubit.dart';

class SortPreferenceState {
  final String sortPreference;

  SortPreferenceState({required this.sortPreference});
}
