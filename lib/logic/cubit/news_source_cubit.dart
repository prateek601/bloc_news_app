import 'package:bloc/bloc.dart';

part 'news_source_state.dart';

class NewsSourceCubit extends Cubit<NewsSourceState> {
  NewsSourceCubit() : super(NewsSourceState(newsSource: []));

  void emitNewsSourceSelected(List _newsSource) =>
      emit(NewsSourceState(newsSource: _newsSource));
}
