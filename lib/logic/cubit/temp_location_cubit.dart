import 'package:bloc/bloc.dart';

part 'temp_location_state.dart';

class TempLocationCubit extends Cubit<TempLocationState> {
  TempLocationCubit() : super(TempLocationState(location: 'India'));

  emitTempLocationSelected(String _location) => emit(TempLocationState(location: _location));
}
