part of 'home_scroll_controller_cubit.dart';

@immutable
abstract class HomeScrollControllerState {}

class NotAtMaxScrollExtent extends HomeScrollControllerState {}

class AtMaxScrollExtent extends HomeScrollControllerState {}
