import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';

part 'home_scroll_controller_state.dart';

class HomeScrollControllerCubit extends Cubit<HomeScrollControllerState> {
  HomeScrollControllerCubit() : super(NotAtMaxScrollExtent());

  void emitAtMaxScrollExtent() => emit(AtMaxScrollExtent());

  void emitNotAtMaxScrollExtent() => emit(NotAtMaxScrollExtent());
}
