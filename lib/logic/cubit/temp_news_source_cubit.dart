import 'package:bloc/bloc.dart';

part 'temp_news_source_state.dart';

class TempNewsSourceCubit extends Cubit<TempNewsSourceState> {
  TempNewsSourceCubit() : super(TempNewsSourceState(newsSource: []));

  void emitTempNewsSource(List _newsSource) =>
      emit(TempNewsSourceState(newsSource: _newsSource));
}
