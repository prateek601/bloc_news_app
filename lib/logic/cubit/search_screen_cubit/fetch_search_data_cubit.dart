import 'package:bloc/bloc.dart';
import 'package:bloc_news_app/data_model/news.dart';
import 'package:bloc_news_app/logic/cubit/search_screen_cubit/search_scroll_controller_cubit.dart';
import 'package:bloc_news_app/utils/http_util.dart';
import 'package:flutter/foundation.dart';

part 'fetch_search_data_state.dart';

class FetchSearchDataCubit extends Cubit<FetchSearchDataState> {
  int page = 1;
  int pageSize = 8;
  String url = '';
  HttpUtil httpUtil = HttpUtil();
  List<Article> articles = [];
  int totalPages = 1;
  SearchScrollControllerCubit searchScrollControllerCubit;

  FetchSearchDataCubit({
    required this.searchScrollControllerCubit,
  }) : super(FetchSearchDataInitial());

  void fetchMoreData() {
    page++;
    if (kDebugMode) {
      print(page);
    }
    if (page <= totalPages) {
      searchScrollControllerCubit.emitAtMaxScrollPosition();
      makeGetRequest();
    }
  }

  void fetchSearchData(String searchQuery) {
    emitFetchSearchDataLoading();
    page = 1;
    url = 'https://newsapi.org/v2/everything?q=$searchQuery';
    makeGetRequest();
  }

  void makeGetRequest() {
    String _pageSize = pageSize.toString();
    String _page = page.toString();
    String completeUrl = url + '&pageSize=$_pageSize&page=$_page';
    if (kDebugMode) {
      print(completeUrl);
    }
    httpUtil.makeGetRequest(
      url: completeUrl,
      onRequestCompleted: (responseBody) {
        News news = News.fromJson(responseBody);
        if (searchScrollControllerCubit.state is AtMaxScrollPosition) {
          articles.addAll(news.articles);
          searchScrollControllerCubit.emitNotAtMaxScrollPosition();
        } else {
          articles.clear();
          articles.addAll(news.articles);
          int totalResults = news.totalResults;
          totalPages = (totalResults / pageSize).ceil();
          if (kDebugMode) {
            print(totalResults);
            print(totalPages);
          }
        }
        emitFetchSearchDataSuccess(articles);
      },
      onRequestFailed: () {
        emitFetchSearchDataError();
      },
    );
  }

  void emitFetchSearchDataSuccess(List<Article> _article) =>
      emit(FetchSearchDataSuccess(article: _article));

  void emitFetchSearchDataLoading() => emit(FetchSearchDataLoading());

  void emitFetchSearchDataError() => emit(FetchSearchDataError());
}
