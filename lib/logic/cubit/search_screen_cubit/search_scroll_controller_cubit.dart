import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'search_scroll_controller_state.dart';

class SearchScrollControllerCubit extends Cubit<SearchScrollControllerState> {
  SearchScrollControllerCubit() : super(NotAtMaxScrollPosition());

  void emitAtMaxScrollPosition() => emit(AtMaxScrollPosition());

  void emitNotAtMaxScrollPosition() => emit(NotAtMaxScrollPosition());
}
