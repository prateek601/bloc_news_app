part of 'fetch_search_data_cubit.dart';

@immutable
abstract class FetchSearchDataState {}

class FetchSearchDataInitial extends FetchSearchDataState {}

class FetchSearchDataSuccess extends FetchSearchDataState {
  final List<Article> article;

  FetchSearchDataSuccess({required this.article});
}

class FetchSearchDataLoading extends FetchSearchDataState {}

class FetchSearchDataError extends FetchSearchDataState {}
