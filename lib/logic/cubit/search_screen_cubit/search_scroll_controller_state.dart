part of 'search_scroll_controller_cubit.dart';

@immutable
abstract class SearchScrollControllerState {}

class NotAtMaxScrollPosition extends SearchScrollControllerState {}

class AtMaxScrollPosition extends SearchScrollControllerState {}
