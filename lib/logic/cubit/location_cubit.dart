import 'package:bloc/bloc.dart';
part 'location_state.dart';

class LocationCubit extends Cubit<LocationState> {
  LocationCubit() : super(LocationState(location: 'India'));

  emitLocationSelected(String _location) =>
      emit(LocationState(location: _location));
}
