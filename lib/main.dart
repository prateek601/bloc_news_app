import 'package:bloc_news_app/logic/cubit/fetch_data_cubit.dart';
import 'package:bloc_news_app/logic/cubit/home_scroll_controller_cubit.dart';
import 'package:bloc_news_app/logic/cubit/location_cubit.dart';
import 'package:bloc_news_app/logic/cubit/news_source_cubit.dart';
import 'package:bloc_news_app/logic/cubit/sort_preference_cubit.dart';
import 'package:bloc_news_app/logic/cubit/temp_location_cubit.dart';
import 'package:bloc_news_app/logic/cubit/temp_news_source_cubit.dart';
import 'package:bloc_news_app/presentation/screens/home_screen/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MultiBlocProvider(
        providers: [
          BlocProvider<TempLocationCubit>(
            create: (context) => TempLocationCubit(),
          ),
          BlocProvider<LocationCubit>(
            create: (context) => LocationCubit(),
          ),
          BlocProvider<TempNewsSourceCubit>(
            create: (context) => TempNewsSourceCubit(),
          ),
          BlocProvider<NewsSourceCubit>(
            create: (context) => NewsSourceCubit(),
          ),
          BlocProvider<HomeScrollControllerCubit>(
            create: (context) => HomeScrollControllerCubit(),
          ),
          BlocProvider<SortPreferenceCubit>(
            create: (context) => SortPreferenceCubit(),
          ),
          BlocProvider<FetchDataCubit>(
            create: (context) => FetchDataCubit(
              locationCubit: context.read<LocationCubit>(),
              newsSourceCubit: context.read<NewsSourceCubit>(),
              homeScrollControllerCubit:
                  context.read<HomeScrollControllerCubit>(),
              sortPreferenceCubit: context.read<SortPreferenceCubit>(),
            ),
          ),
        ],
        child: const Home(),
      ),
    );
  }
}
