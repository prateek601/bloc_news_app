import 'package:bloc_news_app/data_model/news.dart';
import 'package:flutter/material.dart';

class NewsCardsList extends StatelessWidget {
  final List<Article> articles;

  const NewsCardsList({Key? key, required this.articles}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return articles.isEmpty
        ? const Center(
            child: Text(
              'No results available.Try Again',
            ),
          )
        : ListView.builder(
            itemCount: articles.length,
            shrinkWrap: true,
            physics: const ScrollPhysics(),
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: InkWell(
                  onTap: () {},
                  child: Container(
                    height: 140,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.white),
                    child: Padding(
                      padding: const EdgeInsets.all(15),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 3,
                            child: Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        articles[index].source.name,
                                        maxLines: 1,
                                        style: const TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w600,
                                            fontStyle: FontStyle.italic),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        articles[index].title,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 3,
                                      ),
                                    ],
                                  ),
                                  Text(
                                    published(index),
                                    maxLines: 1,
                                    style: const TextStyle(
                                        fontSize: 10, color: Colors.grey),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                              flex: 2,
                              child: Padding(
                                padding: const EdgeInsets.only(left: 10),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(5),
                                  child: Image.network(
                                    articles[index].urlToImage,
                                    fit: BoxFit.cover,
                                    height: double.infinity,
                                  ),
                                ),
                              ))
                        ],
                      ),
                    ),
                  ),
                ),
              );
            });
  }

  String published(int index) {
    DateTime dateTime = DateTime.parse(articles[0].publishedAt);
    var difference = DateTime.now().difference(dateTime);
    if (difference.inDays > 7) {
      return 'a week ago';
    } else if (difference.inDays > 1) {
      return '${difference.inDays} days ago';
    } else if (difference.inDays > 0) {
      return '${difference.inDays} day ago';
    } else if (difference.inHours > 1) {
      return '${difference.inHours} hours ago';
    } else if (difference.inHours > 0) {
      return '${difference.inHours} hour ago';
    } else if (difference.inMinutes > 0) {
      return '${difference.inMinutes} min ago';
    } else {
      return 'a moment ago';
    }
  }
}
