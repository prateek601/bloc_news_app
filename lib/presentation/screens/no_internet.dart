import 'package:flutter/material.dart';

class NoInternet extends StatelessWidget {
  const NoInternet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/no_internet.png',
            height: 120,
            width: 120,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 15, bottom: 20),
            child: Text(
              'No internet Connection!',
              style: TextStyle(color: Colors.grey[700], fontSize: 18),
            ),
          ),
          InkWell(
            onTap: () {},
            child: Container(
              height: 50,
              width: 150,
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(5)),
              child: const Center(
                  child: Text(
                'Try again',
                style: TextStyle(color: Colors.white, fontSize: 16),
              )),
            ),
          )
        ]);
  }
}
