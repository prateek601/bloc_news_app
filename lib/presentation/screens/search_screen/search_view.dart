import 'package:bloc_news_app/logic/cubit/search_screen_cubit/fetch_search_data_cubit.dart';
import 'package:bloc_news_app/logic/cubit/search_screen_cubit/search_scroll_controller_cubit.dart';
import 'package:bloc_news_app/presentation/shared/news_card_list.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../constants.dart';

class SearchView extends StatefulWidget {
  const SearchView({Key? key}) : super(key: key);

  @override
  _SearchViewState createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  ScrollController scrollController = ScrollController();
  TextEditingController textController = TextEditingController();
  late FetchSearchDataCubit fetchSearchDataCubit;

  @override
  void initState() {
    super.initState();
    fetchSearchDataCubit = BlocProvider.of<FetchSearchDataCubit>(context);
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        if (kDebugMode) {
          print('came');
          fetchSearchDataCubit.fetchMoreData();
        }
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    scrollController.dispose();
    textController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: Scaffold(
        appBar: AppBar(
          title: const Text(
            'Search',
            style: TextStyle(color: secondaryColor1, fontSize: 18),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          controller: scrollController,
          child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 30),
                  child: TextField(
                    autofocus: true,
                    textInputAction: TextInputAction.search,
                    controller: textController,
                    cursorColor: Colors.black,
                    style: const TextStyle(fontSize: 18),
                    decoration: InputDecoration(
                      suffixIcon: const Icon(Icons.search),
                      isDense: true,
                      hintText: 'Search for news, topics...',
                      hintStyle: const TextStyle(fontSize: 12),
                      fillColor: secondaryColor2,
                      filled: true,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                        borderSide: const BorderSide(style: BorderStyle.none),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                        borderSide: const BorderSide(style: BorderStyle.none),
                      ),
                    ),
                    onEditingComplete: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      String searchQuery = textController.text;
                      if (searchQuery.trim().isNotEmpty) {
                        fetchSearchDataCubit.fetchSearchData(searchQuery);
                      }
                    },
                  ),
                ),
                BlocBuilder<FetchSearchDataCubit, FetchSearchDataState>(
                  builder: (context, state) {
                    if (state is FetchSearchDataSuccess) {
                      return NewsCardsList(
                        articles: state.article,
                      );
                    } else if (state is FetchSearchDataError) {
                      return const Text('Error occurred');
                    } else if (state is FetchSearchDataLoading) {
                      return const CircularProgressIndicator();
                    } else {
                      return Container();
                    }
                  },
                ),
                BlocBuilder<SearchScrollControllerCubit,
                    SearchScrollControllerState>(
                  builder: (context, state) {
                    if (state is AtMaxScrollPosition) {
                      return const CircularProgressIndicator();
                    } else {
                      return Container();
                    }
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
