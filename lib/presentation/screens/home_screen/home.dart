import 'package:bloc_news_app/constants.dart';
import 'package:bloc_news_app/logic/cubit/fetch_data_cubit.dart';
import 'package:bloc_news_app/logic/cubit/home_scroll_controller_cubit.dart';
import 'package:bloc_news_app/logic/cubit/location_cubit.dart';
import 'package:bloc_news_app/logic/cubit/news_source_cubit.dart';
import 'package:bloc_news_app/logic/cubit/search_screen_cubit/fetch_search_data_cubit.dart';
import 'package:bloc_news_app/logic/cubit/sort_preference_cubit.dart';
import 'package:bloc_news_app/logic/cubit/search_screen_cubit/search_scroll_controller_cubit.dart';
import 'package:bloc_news_app/logic/cubit/temp_location_cubit.dart';
import 'package:bloc_news_app/logic/cubit/temp_news_source_cubit.dart';
import 'package:bloc_news_app/presentation/screens/home_screen/country_list_view.dart';
import 'package:bloc_news_app/presentation/screens/home_screen/bottom_sheet.dart';
import 'package:bloc_news_app/presentation/screens/home_screen/sources_list_view.dart';
import 'package:bloc_news_app/presentation/screens/search_screen/search_view.dart';
import 'package:bloc_news_app/presentation/shared/news_card_list.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  ScrollController scrollController = ScrollController();
  late NewsSourceCubit newsSourceCubit;
  late SortPreferenceCubit sortPreferenceCubit;
  late FetchDataCubit fetchDataCubit;

  @override
  void initState() {
    super.initState();
    newsSourceCubit = BlocProvider.of<NewsSourceCubit>(context);
    sortPreferenceCubit = BlocProvider.of<SortPreferenceCubit>(context);
    fetchDataCubit = BlocProvider.of<FetchDataCubit>(context);
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        if (kDebugMode) {
          print('came');
        }
        BlocProvider.of<FetchDataCubit>(context).monitorScrollController();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            const Text(
              'MyNEWS',
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            InkWell(
              onTap: () => onLocationSelectorTap(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  const Text(
                    'LOCATION',
                    style: TextStyle(fontSize: 10, color: Colors.white),
                  ),
                  Row(
                    children: [
                      const Icon(
                        Icons.location_on,
                        size: 16,
                      ),
                      BlocBuilder<LocationCubit, LocationState>(
                        builder: (context, state) {
                          return Text(
                            state.location,
                            style: const TextStyle(
                              fontSize: 12,
                              color: Colors.white,
                            ),
                          );
                        },
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: onSourceFilterTap,
        backgroundColor: Colors.blue,
        child: Image.asset(
          'assets/filter.png',
          height: 20,
          width: 20,
        ),
        tooltip: 'News source',
      ),
      body: SingleChildScrollView(
        controller: scrollController,
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, bottom: 60),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 30),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => MultiBlocProvider(providers: [
                          BlocProvider(
                              create: (context) =>
                                  SearchScrollControllerCubit()),
                          BlocProvider(
                              create: (context) => FetchSearchDataCubit(
                                  searchScrollControllerCubit: context
                                      .read<SearchScrollControllerCubit>()))
                        ], child: const SearchView()),
                      ),
                    );
                  },
                  child: Container(
                    height: 40,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: BorderRadius.circular(5)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Search for news, topics...',
                            style: TextStyle(
                                fontSize: 12, color: Colors.grey[500]),
                          ),
                          Icon(
                            Icons.search,
                            color: Colors.grey[700],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Top Headlines',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: Colors.grey[700],
                      ),
                    ),
                    Row(
                      children: [
                        Text(
                          'Sort: ',
                          style:
                              TextStyle(fontSize: 12, color: Colors.grey[700]),
                        ),
                        BlocBuilder<SortPreferenceCubit, SortPreferenceState>(
                          builder: (context, state) {
                            return DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                items: sortPreference.map((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(
                                      value,
                                      style: const TextStyle(
                                        color: Colors.grey,
                                      ),
                                    ),
                                  );
                                }).toList(),
                                onChanged: (value) {
                                  if (newsSourceCubit
                                      .state.newsSource.isNotEmpty) {
                                    sortPreferenceCubit
                                        .emitSortPreference(value!);
                                    fetchDataCubit.fetchData();
                                  } else {
                                    showToast();
                                  }
                                },
                                borderRadius: BorderRadius.circular(10),
                                value: state.sortPreference,
                                isDense: true,
                                style: TextStyle(
                                    fontSize: 12, color: Colors.grey[900]),
                              ),
                            );
                          },
                        )
                      ],
                    )
                  ],
                ),
              ),
              BlocBuilder<FetchDataCubit, FetchDataState>(
                builder: (context, state) {
                  if (state is FetchDataSuccess) {
                    return NewsCardsList(
                      articles: state.article,
                    );
                  } else if (state is FetchDataError) {
                    return const Text('Error occurred');
                  } else {
                    return const CircularProgressIndicator();
                  }
                },
              ),
              BlocBuilder<HomeScrollControllerCubit, HomeScrollControllerState>(
                builder: (context, state) {
                  if (state is AtMaxScrollExtent) {
                    return const CircularProgressIndicator();
                  } else {
                    return Container();
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  void onLocationSelectorTap() {
    BlocProvider.of<TempLocationCubit>(context).emitTempLocationSelected(
      BlocProvider.of<LocationCubit>(context).state.location,
    );

    bottomSheet(
      context: context,
      heading: 'Choose your location',
      listWidget: BlocProvider.value(
        value: BlocProvider.of<TempLocationCubit>(context),
        child: BlocProvider.value(
          value: BlocProvider.of<LocationCubit>(context),
          child: const CountryListView(),
        ),
      ),
      buttonName: 'Apply',
      onButtonTap: () {
        String tempLocation =
            BlocProvider.of<TempLocationCubit>(context).state.location;
        BlocProvider.of<LocationCubit>(context)
            .emitLocationSelected(tempLocation);
        // removing all the selected news sources as location and news sources can not
        // clubbed together in api call
        BlocProvider.of<NewsSourceCubit>(context).emitNewsSourceSelected([]);
        BlocProvider.of<FetchDataCubit>(context).fetchData();
      },
    );
  }

  void onSourceFilterTap() {
    BlocProvider.of<TempNewsSourceCubit>(context).emitTempNewsSource(
      BlocProvider.of<NewsSourceCubit>(context).state.newsSource.toList(),
    );

    bottomSheet(
      context: context,
      heading: 'Filter by sources',
      listWidget: BlocProvider.value(
          value: BlocProvider.of<TempNewsSourceCubit>(context),
          child: BlocProvider.value(
              value: BlocProvider.of<NewsSourceCubit>(context),
              child: const SourcesListView())),
      buttonName: 'Apply Filter',
      onButtonTap: () {
        BlocProvider.of<NewsSourceCubit>(context).emitNewsSourceSelected(
          BlocProvider.of<TempNewsSourceCubit>(context)
              .state
              .newsSource
              .toList(),
        );
        BlocProvider.of<FetchDataCubit>(context).fetchData();
      },
    );
  }

  void showToast() {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('Select news source to apply sort preference'),
      ),
    );
  }
}
