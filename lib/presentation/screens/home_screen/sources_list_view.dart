import 'package:bloc_news_app/logic/cubit/temp_news_source_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../constants.dart';

class SourcesListView extends StatelessWidget {
  const SourcesListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TempNewsSourceCubit, TempNewsSourceState>(
      builder: (context, state) {
        return ListView.builder(
          itemCount: newsSources.length,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            bool isChecked;
            List tempNewsSource = state.newsSource;
            if (tempNewsSource.contains(newsSources[index])) {
              isChecked = true;
            } else {
              isChecked = false;
            }
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  newsSources[index],
                  style: TextStyle(
                    color: isChecked ? primaryColor1 : Colors.black,
                  ),
                ),
                Checkbox(
                  checkColor: Colors.white,
                  value: isChecked,
                  onChanged: (bool? value) {
                    if (value == true) {
                      tempNewsSource.add(newsSources[index]);
                      BlocProvider.of<TempNewsSourceCubit>(context)
                          .emitTempNewsSource(tempNewsSource);
                    } else {
                      tempNewsSource.remove(newsSources[index]);
                      BlocProvider.of<TempNewsSourceCubit>(context)
                          .emitTempNewsSource(tempNewsSource);
                    }
                  },
                )
              ],
            );
          },
        );
      },
    );
  }
}
