import 'package:bloc_news_app/logic/cubit/temp_location_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../constants.dart';

class CountryListView extends StatelessWidget {
  const CountryListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TempLocationCubit, TempLocationState>(
      builder: (context, state) {
        return ListView.builder(
            itemCount: countryList.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              bool isChecked = false;
              if (state.location == countryList[index]) {
                isChecked = true;
              }
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    countryList[index],
                    style: TextStyle(
                      color: isChecked ? primaryColor1 : Colors.black,
                    ),
                  ),
                  Radio(
                    value: true,
                    groupValue: isChecked,
                    activeColor: primaryColor1,
                    onChanged: (value) {
                      BlocProvider.of<TempLocationCubit>(context)
                          .emitTempLocationSelected(countryList[index]);
                    },
                  )
                ],
              );
            });
      },
    );
  }
}
