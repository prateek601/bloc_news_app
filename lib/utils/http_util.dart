import 'dart:convert';
import 'package:http/http.dart' as http;

typedef OnRequestCompleted = void Function(Map<String, dynamic> responseBody);
typedef OnRequestFailed = void Function();

class HttpUtil {

  final Map<String, String> _headers = {
    'X-Api-Key': 'f193b8f1536f4d4caa9f6f6a92d5d304'
  };

  Future<void> makeGetRequest({
    required String url,
    required OnRequestCompleted onRequestCompleted,
    required OnRequestFailed onRequestFailed
  }) async {
    try {
      http.Response response =
          await http.get(Uri.parse(url), headers: _headers);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseBody = json.decode(response.body);
        onRequestCompleted(responseBody);
      } else {
        onRequestFailed();
      }
    } catch (e) {
      onRequestFailed();
    }
  }
}